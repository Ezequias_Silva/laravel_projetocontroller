<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Livro extends Model
{

    protected $fillable = [
      'titulo','preco','editora_id'
    ];

  //metodo que acrescenta a relação entre Editora e Livro
  public function editora()
    {
     return $this->belongsTo(Editora::class);
    }

  public function atualiza($request){ //$request significa "pedido"
    $this->fill($request);            //fill significa "encher"
    $this->save();
  }
}
