@extends('layouts.master')

@section('title', 'Cadastro de Editora') <!-- título que vai ficar no navegador -->

@section('pager-header-content', 'Cadastro de Editoras')

@section('content')

  <div class="row">
    <div class="col-md-3">
      <div class="panel panel-primary">
        <div class="panel-heading">
          <h2 class="panel-title">Acões</h2>
        </div>
        <div class="panel-body">
          <a href="/editoras"><span class="glyphycon glyphicon-th-list"></span>Editoras
          </a>
        </div>
      </div>
    </div>
    <div class="col-md-6">
      <form action="/editoras" method="post">
          {{ csrf_field()}} <!--geração de token de segurança do formulario --> 
        <div class="form-group">
          <label for="name">Nome</label>
          <input type="text" name="nome" id="nome" class="form-control">
        </div>

        <div class="form-group">
          <label for="email">Email</label>
          <input type="text" name="email" id="email" class="form-control">
        </div>

        <button class="btn btn-primary">Salvar</button>

      </form>

    </div>

  </div>

@endsection
