@extends('layouts.master')

@section('title','Livros')

@section('pager-header-content','Livros')

@section('content')

  <div class="row">
    <div class="col-md-6">
      <div class="panel panel-primary">
        <div class="panel-heading"><h2 class="panel-title">Ações</h2>
        </div>
        <div class="panel-body">
          <a href="/livros/cria">
            <span class="glyplicon glyphicon-plus" aria-hidden="true"></span>Livro
          </a>

        </div>

        <div class="col-md-6">
          <table class="table table-striped">
            <thead>
              <tr>
                <th>Id</th>
                <th>Título</th>
                <th>Preço</th>
                <th>Editora</th>
                <th>Detalhes</th>
              </tr>
            </thead>

            @foreach ($livros as $livro)

              <tr>
                <td>{{ $livro->id }}</td>
                <td>{{ $livro->titulo }}</td>
                <td>{{ $livro->preco }}</td>
                <td>{{ $livro->editora->nome }}</td>
                <td><a href="/livros/{{ $livro->id }}">Detalhes</a></td>
              </tr>

            @endforeach

          </table>
        </div>

      </div>

    </div>

  </div>

@endsection
