<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//adiciona rota para url '/editoras'
Route::get('/editoras', 'EditoraController@index');

//adicion rota para url '/editora/cria'
Route::get('/editoras/cria', 'EditoraController@cria');

//adiciona rota para armazenar no banco
Route::post('/editoras', 'EditoraController@armazena');


//adiciona rota para url '/livros'
Route::get('/livros', 'LivroController@index');

//adiciona rota para url 'livros/cria'
//obs: até agora ela foi a última rota a ser adicionada, porem ele deve vir antes da rota '/livros/{livro}'
Route::get('/livros/cria','LivroController@cria');

//adiciona rota para armazenar informações no banco
Route::post('/livros','LivroController@armazena');

//adiciona rota para url '/livros/1' por exemplo
Route::get('/livros/{livro}','LivroController@show');

//adiciona rota para a url '/livros/{livro}/edita'
Route::get('/livros/{livro}/edita', 'LivroController@edita');

//ediciona rota para url '/livros/1' por exemplo
Route::patch('/livros/{livro}','LivroController@atualiza');
